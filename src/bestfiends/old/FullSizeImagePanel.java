package bestfiends.game;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.util.Random;

public class FullSizeImagePanel extends JPanel{
    public final static int IMAGE4STARTSCREEN = 99;


    protected boolean imageIconLoaded = false;

    public FullSizeImagePanel(int colorNumber) {
        this.colorNumber = colorNumber;
        setImage(colorNumber);
    }

    public FullSizeImagePanel() {
        this.colorNumber = new Random().nextInt(5) + 1;
        setImage(colorNumber);
    }

    private void setImage(int colorNumber) {
        ImageIcon imageIcon;
        switch (colorNumber) {
            case 0:
                imageIcon = new ImageIcon("src/bestfiends/graphs/black X.png");
                color = new Color(0, 0, 0);
                colorName = "black";
                break;
            case 1:
                imageIcon = new ImageIcon("src/bestfiends/graphs/violet.png");
                color = new Color(255, 0, 255);
                colorName = "purple";
                defaultScaleFactor = 1.4f;
                break;
            case 2:
                imageIcon = new ImageIcon("src/bestfiends/graphs/red.png");
                color = new Color(255, 0, 0);
                colorName = "red";
                defaultScaleFactor = 1.15f;
                break;
            case 3:
                imageIcon = new ImageIcon("src/bestfiends/graphs/green.png");
                color = new Color(0, 255, 0);
                colorName = "green";
                defaultScaleFactor = 1.2f;
                break;
            case 4:
                imageIcon = new ImageIcon("src/bestfiends/graphs/blue.png");
                color = new Color(0, 0, 255);
                colorName = "blue";
                defaultScaleFactor = 1.6f;
                break;
            case 5:
                imageIcon = new ImageIcon("src/bestfiends/graphs/yellow.png");
                color = new Color(255, 255, 0);
                colorName = "yelow";
                defaultScaleFactor = 1.2f;
                break;
            case IMAGE4STARTSCREEN:
                imageIcon = new ImageIcon("src/bestfiends/graphs/large_icon.png");
                color = new Color(255, 255, 255);
                colorName = "start screen";
                break;
            default:
                imageIcon = new ImageIcon("src/bestfiends/graphs/black X.png");
                color = new Color(0, 0, 0);
                colorName = "black";
        }
        imageIconLoaded = imageIcon.getIconWidth() >= 0;
        if (isImageIconLoaded()) {
            image = new BufferedImage(imageIcon.getIconWidth(), imageIcon.getIconHeight(), BufferedImage.TYPE_INT_RGB);
            Graphics g = image.createGraphics();
            imageIcon.paintIcon(null, g, 0, 0);
            g.dispose();
        }
    }



    void setImage() {
        setImage(colorNumber);
    }

    public boolean isImageIconLoaded() {
        return imageIconLoaded;
    }

    private void setPaint() {
        Point2D center = new Point2D.Double(getWidth() / 2, getHeight() / 2);
        float radius = 2f;
        float[] distants = {0f, 1.0f};
        Color[] colors = {getColor(), Color.WHITE};
        paint = new RadialGradientPaint(center, radius, distants, colors, MultipleGradientPaint.CycleMethod.REFLECT);
    }

    private void setPaint(Paint paint) {
        this.paint = paint;
    }
    private Paint getPaint() {
        return paint;
    }

    public BufferedImage getImage() {
        return image;
    }



    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D graphics2D = (Graphics2D) g;
        if (isImageIconLoaded()) {
            //// TODO: 17.03.2016 nowa metoda do ustawiania ustawienia [środek, rozciągnięcie, cały]
            int w = (getWidth() < getHeight() ? getWidth() : getHeight());
            graphics2D.drawImage(getImage(), 0, 0, w, w, null);
        } else {
            paintComponents_GraphError(graphics2D);
        }

    }

    void paintComponents_GraphError(Graphics2D graphics2D) {
        setBackground(color);
        graphics2D.drawString("Graphics file not found", 100, 100);
    }

    public Color getColor() {
        return color;
    }

    public String getColorName() {
        return colorName;
    }

    public boolean equalColor(FullSizeImagePanel p1, FullSizeImagePanel p2) {
        return p1.color.equals(p2.color);
    }

    public float getDefaultScaleFactor() {
        return defaultScaleFactor;
    }

}
