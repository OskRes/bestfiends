package bestfiends.game;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.awt.image.RescaleOp;
import java.util.ArrayDeque;
import java.util.Random;
import java.util.Stack;
import java.util.logging.Logger;

/**
 * Created by Iwona on 02.03.2016.
 * v. 4
 * Panel with game and game algos
 */
public class NewGame_Old_Version extends JPanel {
    private final int ROWS;
    private final int COLLUMNS;
    private final Stack<Ball> selectedBallsStack;
    private final ArrayDeque<ColoredLine2D> linesBetweenBalls;
    private final Ball[][] balls;
    private GameData gameData = null;

    public NewGame_Old_Version(int rows, int collumns, MouseListener childListener, GameData gameData) {
        ROWS = rows;
        COLLUMNS = collumns;
        this.gameData = gameData;
        balls = new Ball[COLLUMNS][ROWS];

        selectedBallsStack = new Stack<>();
        linesBetweenBalls = new ArrayDeque<>();
        MouseListener gameListener = new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (!isGamePossible()) return;
                if (e.getButton() == MouseEvent.BUTTON1 && e.getSource() instanceof Ball && ((Ball) e.getSource()).containInBall(e.getPoint()))
                    button1((Ball) e.getSource());
                else if (e.getButton() == MouseEvent.BUTTON2 && !selectedBallsStack.empty()) button2();
                else if (e.getButton() == MouseEvent.BUTTON3 && selectedBallsStack.size() > 2) {
                    button3();
                    selectedBallsStack.clear();
                    linesBetweenBalls.clear();
                }
            }
        };

        setLayout(new GridBagLayout());
        for (int i = 0; i < balls.length; i++)
            for (int j = 0; j < balls[i].length; j++) {
                balls[i][j] = new Ball(i, j);
                balls[i][j].addMouseListener(gameListener);
                balls[i][j].addMouseListener(childListener);
                add(balls[i][j], new GBC(i, j));
            }
    }

    public GameData getGameData() {
        return gameData;
    }

    private void button1(Ball clickedBall) {
        Logger.getGlobal().entering(getClass().getName(), Thread.currentThread().getName() + ":button1");
        if (selectedBallsStack.empty()) {
            Logger.getGlobal().finest("Button1: adding first ball: " + clickedBall);
            gameData.addPoints();
            selectedBallsStack.push(clickedBall).setSelected(true);
        } else if (selectedBallsStack.peek().equals(clickedBall)) {
            Logger.getGlobal().finest("Button1: removing last ball: " + clickedBall);
            gameData.removePoints();
            linesBetweenBalls.pollLast();
            selectedBallsStack.pop().setSelected(false);
        } else if (selectedBallsStack.peek().isBorderer(clickedBall) && !clickedBall.isSelected() && selectedBallsStack.peek().equalColor(selectedBallsStack.peek(), clickedBall)) {
            Logger.getGlobal().finest("Button1: adding next ball: " + clickedBall);
            gameData.addPoints();
            linesBetweenBalls.add(line(selectedBallsStack.peek(), clickedBall));
            selectedBallsStack.push(clickedBall).setSelected(true);
        }
        repaint();
        Logger.getGlobal().exiting(getClass().getName(), Thread.currentThread().getName() + ":button1");
    }

    private void button2() {
        Logger.getGlobal().entering(getClass().getName(), Thread.currentThread().getName() + ":button2");
        while (!selectedBallsStack.empty()) selectedBallsStack.pop().setSelected(false);
        gameData.clearPoints();
        linesBetweenBalls.clear();
        ColoredLine2D.resetPaint();
        repaint();
        Logger.getGlobal().exiting(getClass().getName(), Thread.currentThread().getName() + ":button2");
    }

    private void button3() {
        Logger.getGlobal().entering(getClass().getName(), Thread.currentThread().getName() + ":button3");
        for (Ball[] ball : balls) {
            Thread thread = new Thread(ball[ball.length - 1]);
            thread.start();
        }
        gameData.endMove();
        ColoredLine2D.resetPaint();
        repaint();
        Logger.getGlobal().exiting(getClass().getName(), Thread.currentThread().getName() + ":button3");
    }

    private int getSmallWidth() {
        return getWidth() / COLLUMNS;
    }

    private int getSmallHeight() {
        return getHeight() / ROWS;
    }

    private ColoredLine2D line(Ball source, Ball target) {
        int x1 = source.getX() + source.getWidth() / 2;
        int y1 = source.getY() + source.getHeight() / 2;
        int x2 = target.getX() + source.getWidth() / 2;
        int y2 = target.getY() + source.getHeight() / 2;
        return new ColoredLine2D(x1, y1, x2, y2);
    }

    public boolean isGamePossible() {
        return gameData.getRemainedMovements() > 0;
    }

    @Override
    protected void paintChildren(Graphics g) {
        super.paintChildren(g);
        Graphics2D d = (Graphics2D) g;
        BasicStroke basicStroke = new BasicStroke(5, BasicStroke.CAP_ROUND, BasicStroke.JOIN_MITER);
        d.setStroke(basicStroke);
        for (ColoredLine2D shape : linesBetweenBalls) {
            d.setPaint(shape.getPaint());
            d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            d.draw(shape);
        }
    }

    public class Ball extends PanelWitchFullSizeImage implements Runnable {
        private int x = -1;
        private int y = -1;
        private boolean select = false;

        public Ball(int x, int y) {
            this(x, y, new Random().nextInt(5) + 1);
        }

        public Ball(int x, int y, int randForColor) {
            super(randForColor);
            this.x = x;
            this.y = y;
            setSelected(false);
            setBorder(new LineBorder(Color.BLACK, 1));
        }

        public void renew() {
            resetImage();
            setSelected(false);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Ball ball = (Ball) o;
            return x == ball.x && y == ball.y && select == ball.select && colorNumber == ball.colorNumber;
        }

        @Override
        public int hashCode() {
            int result = colorNumber;
            result = 31 * result + x;
            result = 31 * result + y;
            result = 31 * result + (select ? 1 : 0);
            return result;
        }

        public int getX_collumn() {
            return x;
        }

        public int getY_row() {
            return y;
        }

        public Color getColor() {
            if (isSelected())
                return originalColor.darker();
            else
                return originalColor;
        }

        public BufferedImage getBufferedImage() {
            if (isSelected()) {
                RescaleOp op = new RescaleOp(getDefaultScaleFactor(), 0, null);
                return op.filter(bufferedImage, null);
            } else
                return bufferedImage;
        }

        public boolean isSelected() {
            return select;
        }

        public void setSelected(boolean select) {
            this.select = select;
        }

        public void setSelected() {
            this.select = !isSelected();
        }

        @Override
        public String toString() {
            return "Ball [" + getX_collumn() + "x" + getY_row() + "] <sel: " + isSelected() + " col: " + getOriginalColorName() + ">";
        }

        public boolean containInBall(Point point) {
            return (drawBall().contains(point));
        }

        private boolean isBorderer(Ball selectedBall) {
            return (Math.abs(getX_collumn() - selectedBall.getX_collumn()) <= 1) &&
                    (Math.abs(getY_row() - selectedBall.getY_row()) <= 1);
        }

        @Override
        public Dimension getPreferredSize() {
            return new Dimension(getSmallWidth(), getSmallHeight());
        }

        private Shape drawBall() {
            double x = getWidth() * 0.1;
            double y = getHeight() * 0.1;
            double w = getWidth() * 0.8;
            double h = getHeight() * 0.8;
            return new Ellipse2D.Double(x, y, w, h);
        }

        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            Graphics2D d = (Graphics2D) g;
            d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            if (isCorrectFilePath()) {
                d.setPaint(new TexturePaint(getBufferedImage(), new Rectangle2D.Double(0, 0, getSmallWidth(), getSmallHeight())));
                d.fill(drawBall());
            } else {
                Point2D center = new Point2D.Double(getWidth() / 2, getHeight() / 2);
                float radius = 2f;
                float[] distants = {0f, 1.0f};
                Color[] colors = {getColor(), Color.WHITE};
                d.setPaint(new RadialGradientPaint(center, radius, distants, colors, MultipleGradientPaint.CycleMethod.REFLECT));
                d.fill(drawBall());
            }
        }

        @Override
        public void run() {
            Logger.getGlobal().entering(getClass().getName(), Thread.currentThread().getName() + ":run(), col: " + getX_collumn());
            for (int i = balls[getX_collumn()].length - 1; i >= 0; i--) balls[getX_collumn()][i].moveBallDown();
            repaint();
            Logger.getGlobal().exiting(getClass().getName(), Thread.currentThread().getName() + ":run(), col: " + getX_collumn());
        }

        private void moveBallDown() {
            if (this.isSelected()) {
                Ball ballToMove = this;
                do {
                    if (ballToMove.isOnTop()) ballToMove.renew();
                    else ballToMove = balls[ballToMove.getX_collumn()][ballToMove.getY_row() - 1];
                } while (ballToMove.isSelected());
                swapBalls(this, ballToMove);
                repaint();
            }
        }

        private void swapBalls(Ball ball, Ball ballToMove) {
            ballToMove.setSelected();
            ball.setSelected();
            ball.colorNumber = ballToMove.colorNumber;
            ball.setImage();
        }

        private boolean isOnTop() {
            return (getY_row() == 0);
        }
    }
}
