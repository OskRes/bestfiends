package bestfiends.game;

import java.util.logging.Logger;

public class ThirdButtonThread implements Runnable {
    final Ball[][] balls;
    final int collumn;

    public ThirdButtonThread(Ball[][] balls, int collumn) {
        this.balls = balls;
        this.collumn = collumn;
    }

    @Override
    public void run() {
        for (int i = balls.length - 1; i >= 0; i--) moveBallDown(balls[collumn][i]);
        Logger.getGlobal().exiting(getClass().getName(), Thread.currentThread().getName() + ":run(), col: " + collumn);
        System.out.println("koniec");
    }

    private void moveBallDown(Ball ball) {
        if (ball.isSelected()) {
            Ball ballToMove = ball;
            do {
                if (ballToMove.getLocationInArray().getY() == 0) ball.setColorNumber();
                else ballToMove = balls[collumn][(int) ballToMove.getLocationInArray().getY() - 1];
            } while (ballToMove.isSelected());
            ball.setColorNumber(ballToMove.getColorNumber());
            ballToMove.setSelected(true);
        }
    }
}
