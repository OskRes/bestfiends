package bestfiends.game;

import bestfiends.libs.ScoreEntry;
import bestfiends.libs.Scores;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.logging.Logger;

public class MasterWindow extends JFrame {
    private static final int WINDOW_WIDTH = 530;
    private static final int WINDOW_HEIGHT = 615;
    private static final String VERSION = "v. 0.8-rebuilding";

    private static final Dimension defaultSize = new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT);
    public static int CENTRUM_EMPTY = 0;
    public static int CENTRUM_GAME = 1;
    public static int CENTRUM_STARTPANEL = 2;
    private final ArrayList<Action> actions4MenuAndButtons = new ArrayList<>();
    private final Scores highscores = new Scores();
    Thread topPanelThread;
    private JDialog informations;
    private BottomPanelWithButtons south = null;
    private JComponent centrum = null;
    private JPanel north = null;
    private MouseListener endGameListener;

    public MasterWindow() {
        super("Klon BestFiends " + VERSION);
        setMinimumSize(defaultSize);
        setResizable(false);
        moveFrameFromTheEdge(100, 100);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                if (saveAllBeforeExit())
                    exitConfirmation(centrum);
            }
        });
        endGameListener = new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (centrum instanceof NewGame_Old_Version && (!((NewGame_Old_Version) centrum).isGamePossible())) {
                    Logger.getGlobal().entering(MasterWindow.class.getName(), "endGame()");
                    endGame();
                }
            }
        };

        makeActionList();
        setJMenuBar(makeMenu());

        south = new BottomPanelWithButtons(actions4MenuAndButtons);
        add(south, BorderLayout.SOUTH);
        replaceCentrum();

        pack();
    }

    private void switchGameAndStartPanel() {
        replaceCentrum();
        validate();
        repaint();
    }

    private void replaceCentrum() {
        if (north != null) {
            ((TopPanel) north).stopPanel();
            remove(north);
            north = null;
        }
        if (getCentrumStatus() == CENTRUM_EMPTY) {
            replaceCentrum_StartScreen();
            return;
        }
        remove(centrum);
        if (getCentrumStatus() == CENTRUM_STARTPANEL) {
            replaceCentrum_NewGame();
        } else if (getCentrumStatus() == CENTRUM_GAME) {
            replaceCentrum_StartScreen();
        }
    }

    private void replaceCentrum_NewGame() {
        centrum = new NewGame_Old_Version(7, 7, endGameListener);
        add(centrum, BorderLayout.CENTER);
        north = new TopPanel(((NewGame_Old_Version) centrum).getGameData());
        add(north, BorderLayout.NORTH);
        topPanelThread = new Thread((TopPanel) north);
        topPanelThread.start();
        south.setTextOnFirstButton(MAINMENU);
    }

    private void replaceCentrum_StartScreen() {
        centrum = new PanelWitchFullSizeImage(PanelWitchFullSizeImage.IMAGE4STARTSCREEN);
        add(centrum, BorderLayout.CENTER);
        south.setTextOnFirstButton(NEWGAME);
    }

    private void endGame() {

        showDialog(NEWHIGHSCORE);
    }

    private int getCentrumStatus() {
        if (centrum == null) return CENTRUM_EMPTY;
        if (centrum instanceof NewGame_Old_Version) return CENTRUM_GAME;
        if (centrum instanceof PanelWitchFullSizeImage) return CENTRUM_STARTPANEL;
        return -1;
    }

    private void makeActionList() {
        actions4MenuAndButtons.add(new AbstractAction(NEWGAME) {
            @Override
            public void actionPerformed(ActionEvent e) {
                switchGameAndStartPanel();
            }
        });
        actions4MenuAndButtons.add(new AbstractAction(SETTINGS) {
            @Override
            public void actionPerformed(ActionEvent e) {
                showDialog(SETTINGS);
            }
        });
        actions4MenuAndButtons.add(new AbstractAction(HIGHSCORE) {
            @Override
            public void actionPerformed(ActionEvent e) {
                showDialog(HIGHSCORE);
            }
        });
        actions4MenuAndButtons.add(new AbstractAction(EXIT) {
            @Override
            public void actionPerformed(ActionEvent e) {
                exitConfirmation(centrum);
            }
        });
    }

    private void showDialog(String typeOfDialog) {
        informations = new JDialog(this, typeOfDialog, true);
        informations.setLocation(new Point((centrum.getWidth() - informations.getWidth()) / 2 + this.getLocation().x, (centrum.getHeight() - informations.getHeight()) / 2 + this.getLocation().y));
        informations.setLayout(new BorderLayout());
        JPanel south = new JPanel();
        informations.add(south, BorderLayout.SOUTH);
        JPanel center = new JPanel();
        informations.add(center, BorderLayout.CENTER);
        informations.setSize(300, 200);

        if (typeOfDialog.equals(NEWHIGHSCORE)) {
            center.add(new JLabel("Podaj swoje imię: "));
            JTextField newScore = new JTextField(12);
            center.add(newScore);
            south.add(new JButton(new AbstractAction(OK) {
                @Override
                public void actionPerformed(ActionEvent e) {
                    ((NewGame_Old_Version) centrum).getGameData().getTotalPoints();
                    highscores.addNewHS(new ScoreEntry(newScore.getText(), ((NewGame_Old_Version) centrum).getGameData().getTotalPoints()));
                    informations.setVisible(false);
                }
            }));
            south.add(new JButton(new AbstractAction(CANCEL) {
                @Override
                public void actionPerformed(ActionEvent e) {
                    informations.setVisible(false);
                }
            }));
            informations.pack();
        } else if (typeOfDialog.equals(HIGHSCORE)) {
            center.setLayout(new GridBagLayout());
            for (int i = 0; i < highscores.getHighScoreEntries().length; i++) {
                center.add(new JLabel("<html><h2>" + highscores.getHighScoreEntries()[i].getName() + "</h2></html>"), new GBC(0, i, 1, 1));
                center.add(new JLabel("<html><h2>" + highscores.getHighScoreEntries()[i].getScore() + "</h2></html>"), new GBC(1, i, 1, 1));

            }

            south.add(new JButton(new AbstractAction(OK) {
                @Override
                public void actionPerformed(ActionEvent e) {
                    informations.setVisible(false);
                }
            }));
            south.add(new JButton(new AbstractAction(RESET) {
                @Override
                public void actionPerformed(ActionEvent e) {
                    clear(informations.getName());
                    informations.setVisible(false);
                }
            }));
            informations.pack();
        } else if (typeOfDialog.equals(SETTINGS)) {
            south.add(new JButton(new AbstractAction(OK) {
                @Override
                public void actionPerformed(ActionEvent e) {
                    informations.setVisible(false);
                }
            }));
            south.add(new JButton(new AbstractAction(RESET) {
                @Override
                public void actionPerformed(ActionEvent e) {
                    clear(informations.getName());
                    informations.setVisible(false);
                }
            }));
        }
        informations.setVisible(true);
    }

    // TODO: 28.02.16 make clear
    private boolean clear(String whichData) {
        switch (whichData) {
            case HIGHSCORE:
                break;
            case SETTINGS:
                break;
            default:
                return false;
        }
        return true;
    }

    private JMenuBar makeMenu() {
        JMenuBar jMenuBar = new JMenuBar();
        JMenu game = jMenuBar.add(new JMenu(MENUNAME));
        for (Action action : actions4MenuAndButtons) game.add(new JMenuItem(action));
        return jMenuBar;
    }

    private void moveFrameFromTheEdge(int x, int y) {
        if (getLocation().getX() < x)
            setLocation((int) getLocation().getX() + x, (int) getLocation().getY());
        if (getLocation().getY() < y)
            setLocation((int) getLocation().getX(), (int) getLocation().getY() + y);
    }

    private boolean saveAllBeforeExit() {
        return true;
    }

    private void exitConfirmation(Component component) {
        if (JOptionPane.showConfirmDialog(component, "Czy napewno chcesz wyjść?", "Potwierdzenie wyjścia", JOptionPane.YES_NO_OPTION) == JOptionPane.OK_OPTION)
            System.exit(0);
    }

    class BottomPanelWithButtons extends JPanel {
        private JButton startbutton;

        public BottomPanelWithButtons(ArrayList<Action> buttonActions) {
            if (!buttonActions.isEmpty()) {
                for (Action action : buttonActions) {
                    if (action.getValue(Action.NAME).equals(NEWGAME)) {
                        startbutton = new JButton(action);
                        add(startbutton);
                    } else add(new JButton(action));
                }
            }
        }

    }
}

