package bestfiends.game;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Iwona on 17.03.2016.
 */
public class ImageStorage {
    ImageSet[] imageSets;

    public ImageStorage() {
        imageSets = new ImageSet[7];
        for (int i = 0; i < imageSets.length; i++) {
            imageSets[i] = new ImageSet(i);
        }
    }

    public static void main(String[] args) {
        ImageStorage imageStorage = new ImageStorage();
        for (int i = 0; i < imageStorage.imageSets.length; i++) {
            System.out.println(imageStorage.imageSets[i].getColorName());
            System.out.println(imageStorage.imageSets[i].getColor());
            System.out.println(imageStorage.imageSets[i].getScaleFactorForImage());
            System.out.println(imageStorage.imageSets[i].getImage());
            System.out.println();
        }
    }

    class ImageSet {
//        public final static int IMAGE4STARTSCREEN = 99;

        private final int number;
        private final ImageIcon image;
        private final float scaleFactorForImage;
        private final Color color;
        private final String colorName;

        public ImageSet(int number) {
            this.number = number;
            switch (number) {
                case 0:
                    image = new ImageIcon("src/bestfiends/graphs/black X.png");
                    scaleFactorForImage = 0f;
                    color = new Color(0, 0, 0);
                    colorName = "black";
                    break;
                case 1:
                    image = new ImageIcon("src/bestfiends/graphs/violet.png");
                    scaleFactorForImage = 1.4f;
                    color = new Color(255, 0, 255);
                    colorName = "purple";
                    break;
                case 2:
                    image = new ImageIcon("src/bestfiends/graphs/red.png");
                    scaleFactorForImage = 1.15f;
                    color = new Color(255, 0, 0);
                    colorName = "red";
                    break;
                case 3:
                    image = new ImageIcon("src/bestfiends/graphs/green.png");
                    scaleFactorForImage = 1.2f;
                    color = new Color(0, 255, 0);
                    colorName = "green";
                    break;
                case 4:
                    image = new ImageIcon("src/bestfiends/graphs/blue.png");
                    scaleFactorForImage = 1.6f;
                    color = new Color(0, 0, 255);
                    colorName = "blue";
                    break;
                case 5:
                    image = new ImageIcon("src/bestfiends/graphs/yellow.png");
                    scaleFactorForImage = 1.2f;
                    color = new Color(255, 255, 0);
                    colorName = "yelow";
                    break;

//                case IMAGE4STARTSCREEN:
                default:
                    image = new ImageIcon("src/bestfiends/graphs/large_icon.png");
                    scaleFactorForImage = 0f;
                    color = new Color(255, 255, 255);
                    colorName = "start screen";
                    break;
            }
        }

        public int getNumber() {
            return number;
        }

        public ImageIcon getImage() {
            return image;
        }

        public float getScaleFactorForImage() {
            return scaleFactorForImage;
        }

        public Color getColor() {
            return color;
        }

        public String getColorName() {
            return colorName;
        }
    }
}
