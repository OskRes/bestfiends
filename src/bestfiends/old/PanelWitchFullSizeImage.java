package bestfiends.game;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Random;
import java.util.logging.Logger;

class PanelWitchFullSizeImage extends JPanel {

    public final static int IMAGE4STARTSCREEN = 99;
    protected int colorNumber;
    protected Color originalColor = null;
    protected String originalColorName = null;
    protected BufferedImage bufferedImage = null;
    protected boolean correctFilePath = false;
    private ImageIcon imageIcon;
    private float defaultScaleFactor = 2f;

    public PanelWitchFullSizeImage(int colorNumber) {
        this.colorNumber = colorNumber;
        setImage(colorNumber);
    }

    public PanelWitchFullSizeImage() {
        this.colorNumber = new Random().nextInt(5) + 1;
        setImage(colorNumber);
    }

    private void setImage(int colorNumber) {
        Logger.getGlobal().entering(this.getClass().getName(), "setImage", colorNumber);
        switch (colorNumber) {
            case 0:
                imageIcon = new ImageIcon("src/bestfiends/graphs/black X.png");
                originalColor = new Color(0, 0, 0);
                originalColorName = "black";
                break;
            case 1:
                imageIcon = new ImageIcon("src/bestfiends/graphs/violet.png");
                originalColor = new Color(255, 0, 255);
                originalColorName = "purple";
                defaultScaleFactor = 1.4f;
                break;
            case 2:
                imageIcon = new ImageIcon("src/bestfiends/graphs/red.png");
                originalColor = new Color(255, 0, 0);
                originalColorName = "red";
                defaultScaleFactor = 1.15f;
                break;
            case 3:
                imageIcon = new ImageIcon("src/bestfiends/graphs/green.png");
                originalColor = new Color(0, 255, 0);
                originalColorName = "green";
                defaultScaleFactor = 1.2f;
                break;
            case 4:
                imageIcon = new ImageIcon("src/bestfiends/graphs/blue.png");
                originalColor = new Color(0, 0, 255);
                originalColorName = "blue";
                defaultScaleFactor = 1.6f;
                break;
            case 5:
                imageIcon = new ImageIcon("src/bestfiends/graphs/yellow.png");
                originalColor = new Color(255, 255, 0);
                originalColorName = "yelow";
                defaultScaleFactor = 1.2f;
                break;
            case IMAGE4STARTSCREEN:
                imageIcon = new ImageIcon("src/bestfiends/graphs/large_icon.png");
                originalColor = new Color(255, 255, 255);
                originalColorName = "start screen";
                break;
            default:
                imageIcon = new ImageIcon("src/bestfiends/graphs/black X.png");
                originalColor = new Color(0, 0, 0);
                originalColorName = "black";
        }
        correctFilePath = imageIcon.getIconWidth() >= 0;
        setBufferedImage();
        Logger.getGlobal().exiting(this.getClass().getName(), "setImage", originalColorName);
    }

    void setImage() {
        setImage(colorNumber);
    }

    void resetImage() {
        colorNumber = new Random().nextInt(5) + 1;
        setImage();
    }

    public void setBufferedImage() {
        if (!isCorrectFilePath()) return;
        bufferedImage = new BufferedImage(imageIcon.getIconWidth(), imageIcon.getIconHeight(), BufferedImage.TYPE_INT_RGB);
        Graphics g = bufferedImage.createGraphics();
        imageIcon.paintIcon(null, g, 0, 0);
        g.dispose();
    }

    public boolean isCorrectFilePath() {
        return correctFilePath;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PanelWitchFullSizeImage that = (PanelWitchFullSizeImage) o;
        return colorNumber == that.colorNumber;
    }

    @Override
    public int hashCode() {
        return colorNumber;
    }

    @Override
    protected void paintComponent(Graphics g) {
        if (colorNumber == IMAGE4STARTSCREEN) {
            Graphics2D graphics2D = (Graphics2D) g;
            int w = (getWidth() < getHeight() ? getWidth() : getHeight());
            graphics2D.drawImage(imageIcon.getImage(), 0, 0, w, w, null);
        } else super.paintComponent(g);
    }

    public Color getOriginalColor() {
        return originalColor;
    }

    public String getOriginalColorName() {
        return originalColorName;
    }

    public boolean equalColor(PanelWitchFullSizeImage p1, PanelWitchFullSizeImage p2) {
        return p1.getOriginalColor().equals(p2.getOriginalColor());
    }

    public float getDefaultScaleFactor() {
        return defaultScaleFactor;
    }
}
