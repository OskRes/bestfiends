package bestfiends.libs;

import java.util.HashMap;

public class Settings_OLD {
    // TODO: 23.02.2016 total hours, total runs, max ranged level, ale bardzo potem
    // dodać jeszcze wszystkie configi
    public static final String PLAYTIME = "playtime";
    public static String PLAYEDGAME = "playedgame";
    public static String HIGHESTLEVEL = "highestLevel";

    private final long startCounting;

    private HashMap<String, Integer> data;

    public Settings_OLD() {
        startCounting = System.currentTimeMillis();
    }

    public void exit() {
        data.put(PLAYTIME, new Integer((int) ((System.currentTimeMillis() - startCounting) / 1000 + data.get(PLAYTIME))));
    }
}
