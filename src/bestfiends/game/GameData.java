package bestfiends.game;

/**
 * Created by sonic on 12.03.16.
 * Class for store points and remained or remainedTime
 */

abstract class GameData {
    private final static int BASIC_POINTS_VALUE = 10;
    protected int remained;
    protected int points4move = 0;
    protected int totalPoints = 0;
    private int point4nextOneBall = BASIC_POINTS_VALUE;

    private GameData(int remained) {
        this.remained = remained;
    }

    static GameData createTimeGameData(int remained) {
        return new GameData(remained) {
            {
                Thread timer = new Thread(() -> {
                    try {
                        while (remained > 0) {
                            Thread.sleep(1000);
                            remained--;
                        }
                    } catch (InterruptedException e) {
                        remained = 0;
                    }
                });
                timer.setName("Thread: Top Panel: Timer");
                timer.start();
            }

            @Override
            public void endMove() {
                totalPoints += points4move;
                clearPoints();
            }

            @Override
            public String getRemainedAsString() {
                return "Pozostało czasu: " + remainedAsMinutesAndSeconds();
            }

            private String remainedAsMinutesAndSeconds() {
                return Integer.toString(remained / 60) + ":" + String.format("%02d", (remained % 60));
            }

        };

    }

    static GameData createMoveGameData(int remained) {
        return new GameData(remained) {
            @Override
            public void endMove() {
                totalPoints += points4move;
                clearPoints();
                remained--;
            }

            @Override
            public String getRemainedAsString() {
                return "Pozostało ruchów: " + remained;
            }
        };
    }

    public abstract void endMove();

    public abstract String getRemainedAsString();

    int getRemained() {
        return remained;
    }

    int getPoints4move() {
        return points4move;
    }

    int getTotalPoints() {
        return totalPoints;
    }

    void addPoints() {
        points4move += point4nextOneBall;
        point4nextOneBall += BASIC_POINTS_VALUE;
    }

    void removePoints() {
        points4move -= (point4nextOneBall -= BASIC_POINTS_VALUE);
    }

    void clearPoints() {
        points4move = 0;
        point4nextOneBall = BASIC_POINTS_VALUE;
    }

    void endGame() {
        remained = 0;
    }

    boolean isGamePossible() {
        return remained > 0;
    }
}
