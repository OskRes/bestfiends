package bestfiends.game;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.awt.image.RescaleOp;
import java.util.Random;
import java.util.logging.Logger;

final class Ball extends JPanel implements Runnable {
    private static int different;
    private final Point2D locationInArray;
    private final Ball upperBall;
    private final Random random = new Random();
    private boolean imageIconLoaded = false;
    private int colorNumber;
    private BufferedImage image = null;
    private Color color = null;
    private String colorName = null;
    private float brightFactor = 0f;
    private boolean select = false;

    Ball(Point2D locationInArray, Ball upperBall, int different) {
        Ball.different = different;
        this.colorNumber = random.nextInt(different) + 1;
        this.locationInArray = locationInArray;
        this.upperBall = upperBall;
        loadImage();
    }

    private void loadImage() {
        ImageIcon imageIcon;
        switch (colorNumber) {
            case 0:
                imageIcon = new ImageIcon(getClass().getResource("/bestfiends/graphs/black X.png"));
                break;
            case 1:
                imageIcon = new ImageIcon(getClass().getResource("/bestfiends/graphs/violet.png"));
                brightFactor = 1.4f;
                break;
            case 2:
                imageIcon = new ImageIcon(getClass().getResource("/bestfiends/graphs/red.png"));
                brightFactor = 1.15f;
                break;
            case 3:
                imageIcon = new ImageIcon(getClass().getResource("/bestfiends/graphs/green.png"));
                brightFactor = 1.2f;
                break;
            case 4:
                imageIcon = new ImageIcon(getClass().getResource("/bestfiends/graphs/blue.png"));
                brightFactor = 1.6f;
                break;
            case 5:
                imageIcon = new ImageIcon(getClass().getResource("/bestfiends/graphs/yellow.png"));
                brightFactor = 1.2f;
                break;
            default:
                imageIcon = new ImageIcon(getClass().getResource("/bestfiends/graphs/black X.png"));
        }
        color = ColorEnum.values()[colorNumber].getColor();
        colorName = ColorEnum.values()[colorNumber].toString();

        imageIconLoaded = imageIcon.getImageLoadStatus() == MediaTracker.COMPLETE;
        if (imageIconLoaded) {
            image = new BufferedImage(imageIcon.getIconWidth(), imageIcon.getIconHeight(), BufferedImage.TYPE_INT_RGB);
            Graphics g = image.createGraphics();
            imageIcon.paintIcon(null, g, 0, 0);
            g.dispose();
        }
    }

    private BufferedImage getImage() {
        if (isSelected()) {
            return new RescaleOp(brightFactor, 0, null).filter(image, null);
        } else return image;
    }

    boolean isSelected() {
        return select;
    }

    void setSelected(boolean select) {
        this.select = select;
    }

    private Color getColor() {
        if (isSelected())
            return color.darker();
        else
            return color;
    }

    boolean sameColor(Ball anotherBall) {
        return this.colorNumber == anotherBall.getColorNumber();
    }

    int getColorNumber() {
        return colorNumber;
    }

    private void setColorNumber(int newColorNumber) {
        setSelected(false);
        if (colorNumber == newColorNumber) return;
        colorNumber = newColorNumber;
        loadImage();
    }

    void setColorNumber() {
        setColorNumber(random.nextInt(different) + 1);
    }

    boolean containInBall(Point point) {
        return (drawBall().contains(point));
    }

    Point2D getLocationInArray() {
        return locationInArray;
    }

    private Shape drawBall() {
        double x = getWidth() * 0.1;
        double y = getHeight() * 0.1;
        double w = getWidth() * 0.8;
        double h = getHeight() * 0.8;
        return new Ellipse2D.Double(x, y, w, h);
    }

    @Override
    public String toString() {
        return "Ball{" +
                "locationInArray=[" + locationInArray.getX() + "x" + locationInArray.getY() +
                "], color=" + colorName +
                ", colornumber=" + colorNumber +
                ", imageIconLoaded=" + imageIconLoaded +
                ", select=" + select +
                ", isOnTop=" + (upperBall == null) +
                '}';
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponents(g);
        Graphics2D graphics2D = (Graphics2D) g;
        if (imageIconLoaded) {
            graphics2D.setPaint(new TexturePaint(getImage(), new Rectangle2D.Double(0, 0, getWidth(), getHeight())));
            graphics2D.fill(drawBall());
        } else {
            Point2D center = new Point2D.Double(getWidth() / 2, getHeight() / 2);
            float radius = 2f;
            float[] distants = {0f, 1.0f};
            Color[] colors = {getColor(), Color.WHITE};
            graphics2D.setPaint(new RadialGradientPaint(center, radius, distants, colors, MultipleGradientPaint.CycleMethod.REFLECT));
            graphics2D.fill(drawBall());
        }
    }

    @Override
    public void run() {
        Logger.getGlobal().entering(getClass().getName(), Thread.currentThread().getName() + ":run(), col: " + getLocationInArray().getX());
        Ball currentBall = this;
        do {
            currentBall = currentBall.moveBallsDown();
        } while (currentBall != null);
        Logger.getGlobal().exiting(getClass().getName(), Thread.currentThread().getName() + ":run(), col: " + getLocationInArray().getX());
    }

    private Ball moveBallsDown() {
        if (isSelected()) {
            Ball ballToMove = this;
            do {
                if (ballToMove.upperBall == null) ballToMove.setColorNumber();
                else ballToMove = ballToMove.upperBall;
            } while (ballToMove.isSelected());
            swapBalls(ballToMove);
        }
        return upperBall;
    }

    private void swapBalls(Ball newBall) {
        newBall.setSelected(true);
        setColorNumber(newBall.getColorNumber());
    }

    enum ColorEnum {
        BLACK(new Color(0, 0, 0)),
        PURPLE(new Color(255, 0, 255)),
        RED(new Color(255, 0, 0)),
        GREEN(new Color(0, 255, 0)),
        BLUE(new Color(0, 0, 255)),
        YELOW(new Color(255, 255, 0));

        private Color color;

        ColorEnum(Color color) {
            this.color = color;
        }

        Color getColor() {
            return color;
        }

        @Override
        public String toString() {
            return super.toString().toLowerCase();
        }
    }
}
