package bestfiends.game;

import bestfiends.libs.GBC;
import bestfiends.libs.NamedThreadFactoryBuilder;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.InputEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Point2D;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.concurrent.*;
import java.util.logging.Logger;

class GameArrayBallsPanel extends JPanel {
    private final int ROWS;
    private final int COLLUMNS;
    private final Ball[][] balls;
    private final Stack<Ball> selectedBallsStack;
    private final ArrayDeque<ColoredLine2D> linesBetweenBalls;
    private final ExecutorService ballsExecutorService;
    private final ExecutorService moveFindersExecutorService;
    private final ExecutorCompletionService<Boolean> moveFindersExecutorCompletionService;
    private final int MINIMUM_BALLS;
    private final int DIFFERENT_COLORS;
    private final GameData gameData;

    GameArrayBallsPanel(int[] settings, MouseListener mouseListener4children, GameData gameData) {
        COLLUMNS = settings[2];
        ROWS = settings[3];
        MINIMUM_BALLS = settings[4];
        DIFFERENT_COLORS = settings[5];
        balls = new Ball[COLLUMNS][ROWS];
        this.gameData = gameData;
        selectedBallsStack = new Stack<>();
        linesBetweenBalls = new ArrayDeque<>();
        ballsExecutorService = Executors.newFixedThreadPool(COLLUMNS, new NamedThreadFactoryBuilder("Thread: Collumn "));
        moveFindersExecutorService = Executors.newFixedThreadPool(DIFFERENT_COLORS, new NamedThreadFactoryBuilder("Thread: MovesFinder "));
        moveFindersExecutorCompletionService = new ExecutorCompletionService<>(moveFindersExecutorService);
        MouseListener gameListener = new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (!gameData.isGamePossible()) return;
                if (e.getButton() == MouseEvent.BUTTON1 && e.getSource() instanceof Ball && ((Ball) e.getSource()).containInBall(e.getPoint()))
                    button1((Ball) e.getSource());
                else if (e.getButton() == MouseEvent.BUTTON2 && !selectedBallsStack.empty()) button2();
                else if (e.getButton() == MouseEvent.BUTTON3 && selectedBallsStack.size() > MINIMUM_BALLS - 1) {
                    button3();
                    selectedBallsStack.clear();
                    linesBetweenBalls.clear();
                }
            }
        };
        setLayout(new GridBagLayout());
        Ball upperBall;
        for (int i = 0; i < balls.length; i++)
            for (int j = 0; j < balls[i].length; j++) {
                if (j == 0) upperBall = null;
                else upperBall = balls[i][j - 1];
                balls[i][j] = new Ball(new Point2D.Float(i, j), upperBall, DIFFERENT_COLORS);
                balls[i][j].addMouseListener(gameListener);
                balls[i][j].addMouseListener(mouseListener4children);
                add(balls[i][j], new GBC(i, j, 1, 1).setFill(GridBagConstraints.BOTH));
                balls[i][j].setBorder(new LineBorder(Color.BLACK, 1));
            }
        checkPossibleMoves(0);
    }

    private void checkPossibleMoves(int i) {
        i++;
        if (i > 5) {
            gameData.endGame();
            balls[0][0].getListeners(MouseListener.class)[0].mouseClicked(new MouseEvent(this, 0, 0, InputEvent.BUTTON1_MASK, 0, 0, 1, false));
            Logger.getGlobal().warning("Warning. Can't find array with possible move. Please check your settings. Make array bigger, make number of color or minimum balls for move smaller ");
        }
        if (!isNextMovePossible()) {
            randomizeBalls();
            checkPossibleMoves(i);
        }
    }

    private void randomizeBalls() {
        for (int i = 0; i < balls.length; i++)
            for (int j = 0; j < balls[i].length; j++) balls[i][j].setColorNumber();
    }

    private void button1(Ball clickedBall) {
        Logger.getGlobal().entering(getClass().getName(), Thread.currentThread().getName() + ":button1");
        if (selectedBallsStack.empty()) {
            Logger.getGlobal().fine("Button1: adding first ball: " + clickedBall);
            gameData.addPoints();
            selectedBallsStack.push(clickedBall).setSelected(true);
        } else if (selectedBallsStack.peek().equals(clickedBall)) {
            Logger.getGlobal().fine("Button1: removing last ball: " + clickedBall);
            gameData.removePoints();
            linesBetweenBalls.pollLast();
            selectedBallsStack.pop().setSelected(false);
        } else if (isBorderer(selectedBallsStack.peek(), clickedBall) && !clickedBall.isSelected() && selectedBallsStack.peek().sameColor(clickedBall)) {
            Logger.getGlobal().fine("Button1: adding next ball: " + clickedBall);
            gameData.addPoints();
            linesBetweenBalls.add(line(selectedBallsStack.peek(), clickedBall));
            selectedBallsStack.push(clickedBall).setSelected(true);
        }
        repaint();
        Logger.getGlobal().exiting(getClass().getName(), Thread.currentThread().getName() + ":button1");
    }

    private void button2() {
        Logger.getGlobal().entering(getClass().getName(), Thread.currentThread().getName() + ":button2");
        while (!selectedBallsStack.empty()) selectedBallsStack.pop().setSelected(false);
        gameData.clearPoints();
        linesBetweenBalls.clear();
        ColoredLine2D.resetPaint();
        repaint();
        Logger.getGlobal().exiting(getClass().getName(), Thread.currentThread().getName() + ":button2");
    }

    private void button3() {
        Logger.getGlobal().entering(getClass().getName(), Thread.currentThread().getName() + ":button3");
        // TODO: 07.04.2016 może potem zastąpić ExecutorService przez ExecutionCompletionService
        List<Future<?>> results = new ArrayList<>();
        for (Ball[] ballsCollumn : balls) {
            Logger.getGlobal().fine("Thread: Collumn " + (int) ballsCollumn[ballsCollumn.length - 1].getLocationInArray().getX() + " " + ballsCollumn[ballsCollumn.length - 1]);
            Thread thread = new Thread(ballsCollumn[ballsCollumn.length - 1]);
            thread.setName("Thread: Kolumna " + (int) ballsCollumn[ballsCollumn.length - 1].getLocationInArray().getX());
            results.add(ballsExecutorService.submit(ballsCollumn[ballsCollumn.length - 1]));
            thread.start();
        }
        for (int i = 0; i < results.size(); ) if (results.get(i).isDone()) i++;

        gameData.endMove();
        ColoredLine2D.resetPaint();
        if (!isNextMovePossible()) {
            gameData.endGame();
            balls[0][0].getListeners(MouseListener.class)[0].mouseClicked(new MouseEvent(this, 0, 0, InputEvent.BUTTON1_MASK, 0, 0, 1, false));
        }
        repaint();
        Logger.getGlobal().exiting(getClass().getName(), Thread.currentThread().getName() + ":button3");
    }

    private ColoredLine2D line(Ball source, Ball target) {
        int x1 = source.getX() + source.getWidth() / 2;
        int y1 = source.getY() + source.getHeight() / 2;
        int x2 = target.getX() + source.getWidth() / 2;
        int y2 = target.getY() + source.getHeight() / 2;
        return new ColoredLine2D(x1, y1, x2, y2);
    }

    private boolean isBorderer(Ball b1, Ball b2) {
        return b1.getLocationInArray().distance(b2.getLocationInArray()) < 2;
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(COLLUMNS * getBallDiagonal(), ROWS * getBallDiagonal());
    }

    private int getBallDiagonal() {
        int w, h, c, r;
        w = Toolkit.getDefaultToolkit().getScreenSize().width;
        h = Toolkit.getDefaultToolkit().getScreenSize().height;
        c = (int) (w * 0.6 / COLLUMNS);
        r = (int) (h * 0.7 / ROWS);
        if (c > 100) c = 100;
        if (r > 100) c = 100;
        return (c < r ? c : r);
    }


    @Override
    protected void paintComponent(Graphics g) {
        Graphics2D graphics2D = (Graphics2D) g;
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        super.paintComponent(graphics2D);
    }

    @Override
    protected void paintChildren(Graphics g) {
        super.paintChildren(g);
        Graphics2D graphics2D = (Graphics2D) g;
        BasicStroke basicStroke = new BasicStroke(5, BasicStroke.CAP_ROUND, BasicStroke.JOIN_MITER);
        graphics2D.setStroke(basicStroke);
        for (ColoredLine2D line2D : linesBetweenBalls) {
            graphics2D.setPaint(line2D.getPaint());
            graphics2D.draw(line2D);
        }
    }

    void endGame() {
        ballsExecutorService.shutdown();
        moveFindersExecutorService.shutdown();
    }

    private boolean isNextMovePossible() {
        int[][] ballsArrayAsInts = PossibleMoveFinder.convertBallToInt(balls);
        Stack<Future<Boolean>> results = new Stack<>();
        try {
            for (int i = 0; i < DIFFERENT_COLORS; i++) {
                Logger.getGlobal().fine("Thread: MovesFinder " + (i + 1));
                results.push(moveFindersExecutorCompletionService.submit(new PossibleMoveFinder(ballsArrayAsInts, i + 1, MINIMUM_BALLS)));
            }
            for (Future<Boolean> future : results) {
                try {
                    if (!future.isCancelled() && future.get()) return true;
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                }
            }
        } finally {
            while (!results.empty()) results.pop().cancel(true);
        }
        return false;
    }

    private static class PossibleMoveFinder implements Callable<Boolean> {
        private final int minimum;
        private int[][] array;
        private int color;
        private List<Point> selected = new ArrayList<>();

        PossibleMoveFinder(int[][] ballsToCompute, int colors, int minimum) {
            array = ballsToCompute;
            color = colors;
            this.minimum = minimum;
        }

        private static int[][] convertBallToInt(Ball[][] balls) {
            int[][] tmp = new int[balls[0].length][balls.length];
            for (int i = 0; i < balls.length; i++)
                for (int j = 0; j < balls[i].length; j++)
                    tmp[j][i] = balls[i][j].getColorNumber();
            return tmp;
        }

        static void showArray(int[][] array) {
            System.out.println("--------------");
            for (int i = 0; i < array.length; i++) {
                for (int j = 0; j < array[i].length; j++)
                    System.out.print(array[i][j] + " ");
                System.out.println();
            }

        }

        @Override
        public Boolean call() throws Exception {
            for (int i = 0; i < array.length; i++)
                for (int j = 0; j < array[i].length; j++)
                    if ((array[i][j] == color) && moveExist(i, j)) return true;
            return false;
        }

        private boolean moveExist(int x, int y) {
            selected.add(new Point(x, y));
            array[x][y] = 0;
            if (selected.size() >= minimum) return true;
            else {
                int nextX;
                int nextY;
                for (int i = -1; i < 2; i++)
                    for (int j = -1; j < 2; j++) {
                        nextX = x + i;
                        nextY = y + j;
                        if (isInBounds(nextX, nextY) && isSameColor(nextX, nextY) && moveExist(nextX, nextY))
                            return true;
                    }
                selected.remove(selected.size() - 1);
                array[x][y] = color;
                return false;
            }
        }

        private boolean isInBounds(int nextX, int nextY) {
            return ((nextX >= 0) && (nextY >= 0) && (nextX < array.length) && (nextY < array[nextX].length));
        }

        private boolean isSameColor(int nextX, int nextY) {
            return (array[nextX][nextY] == color);
        }
    }
}