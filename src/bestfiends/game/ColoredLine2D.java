package bestfiends.game;

import java.awt.*;
import java.awt.geom.Line2D;

/**
 * Created by Iwona on 10.03.2016.
 * class for lines
 */
class ColoredLine2D extends Line2D.Float {
    private final static Color[] colors = {new Color(0xff), new Color(0x1700e7), new Color(0x2e00d0), new Color(0x4500b9), new Color(0x5c00a2), new Color(0x73008b), new Color(0x8b0073), new Color(0xa2005c), new Color(0xb90045), new Color(0xd0002e), new Color(0xae70017), new Color(0xff0000)};
    private static int paintNR = 0;
    private Paint paint;

    ColoredLine2D(float x1, float y1, float x2, float y2) {
        super(x1, y1, x2, y2);
        setPaint(x1, y1, x2, y2);
    }

    static void resetPaint() {
        paintNR = 0;
    }

    Paint getPaint() {
        return paint;
    }

    private void setPaint(float x1, float y1, float x2, float y2) {
        getStartColor();
        paint = new GradientPaint(x1, y1, getStartColor(), x2, y2, getEndColor());
    }

    private Color getStartColor() {
        return colors[paintNR];
    }

    private Color getEndColor() {
        if (paintNR + 1 < colors.length) paintNR++;
        return colors[paintNR];
    }
}
