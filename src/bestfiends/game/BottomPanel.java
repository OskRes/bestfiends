package bestfiends.game;

import javax.swing.*;
import java.util.ArrayList;

class BottomPanel extends JPanel {

    BottomPanel(ArrayList<Action> buttonActions) {
        if (!buttonActions.isEmpty()) for (Action action : buttonActions) add(new JButton(action));
    }

    void renameButton(String newText) {
        renameButton(0, newText);
    }

    private void renameButton(int i, String newText) {
        if (i < getComponents().length && getComponent(i) instanceof JButton) {
            ((JButton) getComponent(i)).setText(newText);
        }
    }
}
