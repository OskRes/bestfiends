package bestfiends.game;

import bestfiends.libs.Dictionary;
import bestfiends.libs.GBC;
import bestfiends.libs.Settings;

import javax.swing.*;
import javax.swing.border.EtchedBorder;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.util.Properties;
import java.util.Random;

class SettingsPanel extends JPanel {
    private final JPanel ballsArraySizePanel;
    private final JPanel minBallsPanel;
    private final JPanel colorsPanel;
    private final JDialog jDialog;
    private final JPanel gameModeSettingsPanelTime;
    private final JPanel gameModeSettingsPanelMoves;
    private final Properties newSettings;
    private final Properties maxima;
    private final JPanel gameModePanel;
    private JPanel gameModeSettingsPanel;

    SettingsPanel(JDialog jDialog, Properties newSettings, Properties maxima) {
        this.newSettings = newSettings;
        this.maxima = maxima;
        this.jDialog = jDialog;

        setLayout(new GridBagLayout());

        gameModeSettingsPanelTime = makePanel(Dictionary.MAXTIME_FRAME, new GridBagLayout());
        addSliderAndInformation(gameModeSettingsPanelTime, makeSlider(30, Integer.valueOf(maxima.getProperty(Settings.MAX_TIME)), Integer.valueOf(newSettings.getProperty(Settings.MAX_TIME)), 30), Settings.MAX_TIME);

        gameModeSettingsPanelMoves = makePanel(Dictionary.MAXMOVES_FRAME, new GridBagLayout());
        addSliderAndInformation(gameModeSettingsPanelMoves, makeSlider(2, Integer.valueOf(maxima.getProperty(Settings.MAX_MOVES)), Integer.valueOf(newSettings.getProperty(Settings.MAX_MOVES)), 2), Settings.MAX_MOVES);

        minBallsPanel = makePanel(Dictionary.MINBALLS_FRAME, new GridBagLayout());
        addSliderAndInformation(minBallsPanel, makeSlider(3, Integer.valueOf(maxima.getProperty(Settings.MINIMUM_BALLS)), Integer.valueOf(newSettings.getProperty(Settings.MINIMUM_BALLS)), 1), Settings.MINIMUM_BALLS);

        colorsPanel = makePanel(Dictionary.BALLS_COLOR_FRAME, new GridBagLayout());
        JSlider colorPanelSlider = makeSlider(3, Integer.valueOf(maxima.getProperty(Settings.DIFFERENT_COLORS)), Integer.valueOf(newSettings.getProperty(Settings.DIFFERENT_COLORS)), 1);
        colorPanelSlider.addChangeListener(e -> repaint());
        addSliderAndInformation(colorsPanel, colorPanelSlider, Settings.DIFFERENT_COLORS);

        gameModePanel = makePanel(Dictionary.GAME_MODE_FRAME, new GridLayout(0, 1));
        makeGameModePanel();

        ballsArraySizePanel = makePanel(Dictionary.SIZE_FRAME, new GridBagLayout());
        makeBallsArraySizePanel();

        add(gameModePanel, new GBC(0, 0, 6, 3).setFill(GridBagConstraints.BOTH));
        add(gameModeSettingsPanel, new GBC(6, 0, 6, 3).setFill(GridBagConstraints.BOTH));
        add(ballsArraySizePanel, new GBC(0, 3, 6, 8).setFill(GridBagConstraints.BOTH));
        add(minBallsPanel, new GBC(6, 3, 6, 4).setFill(GridBagConstraints.BOTH));
        add(colorsPanel, new GBC(6, 7, 6, 4).setFill(GridBagConstraints.BOTH));
    }

    private JPanel makePanel(String borderTitle, LayoutManager layout) {
        JPanel jPanel = new JPanel(layout);
        jPanel.setName(borderTitle);
        jPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED), borderTitle));
        return jPanel;
    }

    private void makeGameModePanel() {
        ButtonGroup gameModeButtonGroup = new ButtonGroup();
        JRadioButton gameModeMoves = new JRadioButton(Dictionary.GAME_MODE_MOVES);
        JRadioButton gameModeTime = new JRadioButton(Dictionary.GAME_MODE_TIME);
        if (newSettings.getProperty(Settings.SET_GAME_MODE).equals(Settings.GAME_MODE_MOVES)) {
            gameModeMoves.setSelected(true);
            gameModeSettingsPanel = gameModeSettingsPanelMoves;
        } else if (newSettings.getProperty(Settings.SET_GAME_MODE).equals(Settings.GAME_MODE_TIME)) {
            gameModeTime.setSelected(true);
            gameModeSettingsPanel = gameModeSettingsPanelTime;
        }

        gameModeMoves.addActionListener(changeGameModeSettingsPanelAction(Settings.GAME_MODE_MOVES, gameModeSettingsPanelMoves));
        gameModeTime.addActionListener(changeGameModeSettingsPanelAction(Settings.GAME_MODE_TIME, gameModeSettingsPanelTime));

        gameModeButtonGroup.add(gameModeMoves);
        gameModeButtonGroup.add(gameModeTime);
        gameModePanel.add(gameModeMoves);
        gameModePanel.add(gameModeTime);
    }

    private void makeBallsArraySizePanel() {
        JSlider sliderX = makeSlider(2, Integer.valueOf(maxima.getProperty(Settings.ARRAY_SIZE_WIDTH)), Integer.valueOf(newSettings.getProperty(Settings.ARRAY_SIZE_WIDTH)), 1);
        JSlider sliderY = makeSlider(2, Integer.valueOf(maxima.getProperty(Settings.ARRAY_SIZE_HEIGHT)), Integer.valueOf(newSettings.getProperty(Settings.ARRAY_SIZE_HEIGHT)), 1);
        sliderY.setOrientation(SwingConstants.VERTICAL);

        DraftLabel draft = new DraftLabel(sliderX.getValue(), sliderY.getValue(), Integer.valueOf(newSettings.getProperty(Settings.DIFFERENT_COLORS)));
        JLabel labelX = new JLabel(Integer.toString(sliderX.getValue()));
        labelX.setFont(new Font(labelX.getFont().getName(), labelX.getFont().getStyle(), 16));
        labelX.setPreferredSize(new Dimension(40, 40));
        JLabel labelY = new JLabel(Integer.toString(sliderY.getValue()));
        labelY.setFont(new Font(labelY.getFont().getName(), labelY.getFont().getStyle(), 16));
        labelY.setPreferredSize(new Dimension(40, 40));
        sliderX.addChangeListener(e -> {
            labelX.setText(Integer.toString(sliderX.getValue()));
            newSettings.setProperty(Settings.ARRAY_SIZE_WIDTH, Integer.toString(sliderX.getValue()));
            draft.setBallsInColls(sliderX.getValue());
            draft.repaint();
        });
        sliderY.addChangeListener(e -> {
            labelY.setText(Integer.toString(sliderY.getValue()));
            newSettings.setProperty(Settings.ARRAY_SIZE_HEIGHT, Integer.toString(sliderY.getValue()));
            draft.setBallsInRows(sliderY.getValue());
            draft.repaint();
        });
        ballsArraySizePanel.add(sliderY, new GBC(0, 0, 1, 5));
        ballsArraySizePanel.add(draft, new GBC(1, 0, 5, 5).setFill(GridBagConstraints.BOTH));
        ballsArraySizePanel.add(sliderX, new GBC(1, 5, 5, 1));
//        ballsArraySizePanel.add(labelX, new GBC(1, 6, 2, 2));
//        ballsArraySizePanel.add(labelY, new GBC(4, 6, 2, 2));
    }

    private void addSliderAndInformation(JPanel panel, JSlider slider, String keyToModify) {
        JLabel value = new JLabel(Integer.toString(slider.getValue()), SwingConstants.CENTER);
        value.setFont(new Font(value.getFont().getName(), value.getFont().getStyle(), 16));
        value.setPreferredSize(new Dimension(40, 40));
        slider.addChangeListener(e -> {
            if (slider.getValue() % slider.getMajorTickSpacing() == 0) {
                value.setText(Integer.toString(slider.getValue()));
                newSettings.setProperty(keyToModify, Integer.toString(slider.getValue()));
            }
        });
        panel.add(slider, new GBC(0, 0, 4, 1));
//        panel.add(value, new GBC(4, 0, 1, 1));
    }

    private JSlider makeSlider(int minimalValue, int maximalValue, int defaultValue, int tickSize) {
        JSlider slider = new JSlider(minimalValue, maximalValue, defaultValue);
        slider.setMajorTickSpacing(tickSize);
        slider.setPaintTicks(true);
        slider.setPaintLabels(true);
        slider.setLabelTable(slider.createStandardLabels(tickSize, minimalValue));
        slider.setSnapToTicks(true);
        return slider;
    }

    private ActionListener changeGameModeSettingsPanelAction(String gameMode, JPanel gameModePanel) {
        return e -> {
            newSettings.setProperty(Settings.SET_GAME_MODE, gameMode);
            remove(gameModeSettingsPanel);
            gameModeSettingsPanel = gameModePanel;
            add(gameModeSettingsPanel, new GBC(6, 0, 6, 3).setFill(GridBagConstraints.BOTH));
            jDialog.revalidate();
            jDialog.repaint();
        };
    }

    Properties getNewSettings() {
        return newSettings;
    }

    private class DraftLabel extends JComponent {
        private final static int spaceFromEdge = 20;
        Random random = new Random();
        private int ballsInRow;
        private int ballsInColls;
        private int colors;

        private DraftLabel(int ballsInColls, int ballsInRow, int colors) {
            setBorder(BorderFactory.createEtchedBorder());
            this.ballsInRow = ballsInRow;
            this.ballsInColls = ballsInColls;
            this.colors = colors;
        }

        private void setBallsInRows(int ballsInRow) {
            this.ballsInRow = ballsInRow;
        }

        private void setBallsInColls(int ballsInColls) {
            this.ballsInColls = ballsInColls;
        }

        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            Graphics2D graphics2D = (Graphics2D) g;
            graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            int d;
            if (ballsInColls > ballsInRow) d = (getWidth() - spaceFromEdge) / ballsInColls;
            else d = (getHeight() - spaceFromEdge) / ballsInRow;
            colors = ((JSlider) colorsPanel.getComponent(0)).getValue();
            for (int c = 0; c < ballsInColls; c++) {
                for (int r = 0; r < ballsInRow; r++) {
                    graphics2D.setPaint(Ball.ColorEnum.values()[random.nextInt(colors) + 1].getColor());
                    graphics2D.fill(new Ellipse2D.Float((getWidth() - d * ballsInColls) / 2 + d * c, (getHeight() - d * ballsInRow) / 2 + d * r, d, d));
                    graphics2D.setPaint(Color.BLACK);
                    graphics2D.draw(new Rectangle2D.Float((getWidth() - d * ballsInColls) / 2 + d * c, (getHeight() - d * ballsInRow) / 2 + d * r, d, d));
                }
            }
        }
    }
}
