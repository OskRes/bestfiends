package bestfiends.game;

import bestfiends.libs.Dictionary;
import bestfiends.libs.GBC;
import bestfiends.libs.ScoreEntry;
import bestfiends.libs.Settings;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import java.awt.*;
import java.awt.event.*;
import java.awt.font.FontRenderContext;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.logging.Logger;

/**
 * Created by Iwona on 02.03.2016.
 * Okno główne
 */
public class MasterWindow extends JFrame {
    private static final String VERSION = "v. 0.14-sizeArrayDependent";
    private static final String gamePatch = System.getProperty("user.dir") + "/data/";
    private final Settings settings = new Settings(gamePatch);
    private final ArrayList<Action> actions4MenuAndButtons = new ArrayList<>();
    private final StartScreenPanel centrumStartScreen = new StartScreenPanel(StartScreenPanel.STRETCH);
    private final MouseListener endGameListenerMouseListener;
    private NewGamePanel centrumGame = null;
    private JPanel centrum = null;
    private BottomPanel south = null;

    public MasterWindow() {
        super("Klon BestFiends " + VERSION);

        setResizable(false);
        makeActionList();
        setJMenuBar(makeMenu());

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                exitConfirmation(centrum);
            }
        });
        endGameListenerMouseListener = new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (centrum instanceof NewGamePanel && (!((NewGamePanel) centrum).isGamePossible())) {
                    Logger.getGlobal().entering(MasterWindow.class.getName(), "endGame()");
                    endGame();
                }
            }
        };

        south = new BottomPanel(actions4MenuAndButtons);
        add(south, BorderLayout.SOUTH);

        startStartScreen();
    }

    private void moveFrameToCenter() {
        Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation(new Point((screen.width - getWidth()) / 2, (screen.height - getHeight()) / 2));
    }

    private void makeActionList() {
        actions4MenuAndButtons.add(new AbstractAction(Dictionary.NEWGAME) {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (centrum instanceof NewGamePanel) startStartScreen();
                else if (centrum instanceof StartScreenPanel) startNewGame();
            }
        });
        actions4MenuAndButtons.add(new AbstractAction(Dictionary.SETTINGS) {
            @Override
            public void actionPerformed(ActionEvent e) {
                showDialog(Dictionary.SETTINGS);
            }
        });
        actions4MenuAndButtons.add(new AbstractAction(Dictionary.HIGHSCORE) {
            @Override
            public void actionPerformed(ActionEvent e) {
                showDialog(Dictionary.HIGHSCORE);
            }
        });
        actions4MenuAndButtons.add(new AbstractAction(Dictionary.EXIT) {
            @Override
            public void actionPerformed(ActionEvent e) {
                exitConfirmation(centrum);
            }
        });
    }

    private JMenuBar makeMenu() {
        JMenuBar jMenuBar = new JMenuBar();
        JMenu game = jMenuBar.add(new JMenu(Dictionary.MENUNAME));
        for (Action action : actions4MenuAndButtons) game.add(new JMenuItem(action));
        return jMenuBar;
    }

    private void startNewGame() {
        removeCentrum();
        renameStartButton(Dictionary.MAINMENU);
        centrumGame = new NewGamePanel(settings.getSettings(), endGameListenerMouseListener);
        add(centrumGame, BorderLayout.CENTER);
        centrum = centrumGame;
        pack();
        moveFrameToCenter();
    }

    private void startStartScreen() {
        removeCentrum();
        renameStartButton(Dictionary.NEWGAME);
        add(centrumStartScreen, BorderLayout.CENTER);
        centrum = centrumStartScreen;
        pack();
        moveFrameToCenter();
    }

    private synchronized void endGame() {
        if (centrum instanceof NewGamePanel)
            if (settings.getScores().isNewHighScore(centrumGame.data.getTotalPoints()))
                showDialog(Dictionary.NEWHIGHSCORE);
        settings.saveScoresToFile();
        startStartScreen();
    }

    private void removeCentrum() {
        if (centrum != null) {
            remove(centrum);
            if (centrum instanceof NewGamePanel) centrumGame.stopGameThreads();
        }
        centrum = null;
    }

    private void renameStartButton(String newText) {
        if (newText.equals(Dictionary.NEWGAME) || newText.equals(Dictionary.MAINMENU)) {
            south.renameButton(newText);
        }
    }

    private void showDialog(String typeOfDialog) {
        JDialog informations = new JDialog(this, typeOfDialog, true);
        informations.setResizable(false);
        informations.setLayout(new BorderLayout());
        JPanel center = new JPanel();
        JPanel south = new JPanel();
        switch (typeOfDialog) {
            case Dictionary.NEWHIGHSCORE:
                center.add(new JLabel("Podaj swoje imię: "));
                JTextField newScore = new JTextField(12);
                center.add(newScore);
                south.add(new JButton(new AbstractAction(Dictionary.OK) {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        settings.getScores().addNewHS(new ScoreEntry(newScore.getText(), centrumGame.data.getTotalPoints()));
                        informations.setVisible(false);
                    }
                }));
                south.add(new JButton(new AbstractAction(Dictionary.CANCEL) {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        informations.setVisible(false);
                    }
                }));
                informations.pack();
                break;
            case Dictionary.HIGHSCORE:
                if (settings.getScores().isEmpty()) {
                    JLabel emptyHighScoreInfo = new JLabel(Dictionary.HIGHSCORE_IS_EMPTY, SwingConstants.CENTER);
                    emptyHighScoreInfo.setFont(new Font(emptyHighScoreInfo.getFont().getName(), Font.BOLD, 24));
                    FontRenderContext fontRenderContext = ((Graphics2D) getGraphics()).getFontRenderContext();
                    Rectangle2D rectangle2D = emptyHighScoreInfo.getFont().getStringBounds(Dictionary.HIGHSCORE_IS_EMPTY, fontRenderContext);
                    emptyHighScoreInfo.setPreferredSize(new Dimension((int) (rectangle2D.getWidth() + 60), (int) rectangle2D.getHeight() + 10));
                    center.add(emptyHighScoreInfo);
                } else {
                    center.setLayout(new GridLayout(0, 1));
                    JPanel title = new JPanel(new GridBagLayout());
                    title.add(new JLabel("<html><h3>Poz.</h3></html>", SwingConstants.CENTER) {
                        @Override
                        public Dimension getPreferredSize() {
                            return new Dimension(30, 40);
                        }
                    }, new GBC(0, 0, 1, 1).setFill(GBC.BOTH).setWeight(0, 0));
                    title.add(new JLabel("<html><h3>Gracz</h3></html>", SwingConstants.CENTER) {
                        @Override
                        public Dimension getPreferredSize() {
                            return new Dimension(130, 40);
                        }
                    }, new GBC(1, 0, 2, 1).setFill(GBC.BOTH).setWeight(1, 0));
                    title.add(new JLabel("<html><h3>Punkty</h3></html>", SwingConstants.CENTER) {
                        @Override
                        public Dimension getPreferredSize() {
                            return new Dimension(60, 40);
                        }
                    }, new GBC(3, 0, 1, 1).setFill(GBC.BOTH).setWeight(0, 0).setInsetsH(0, 20));
                    center.add(title);
                    for (int i = 0; i < settings.getScores().getHighScoreEntries().length; i++) {
                        JPanel jPanel = new JPanel(new GridBagLayout());
                        jPanel.setBorder(new BevelBorder(BevelBorder.RAISED));
                        jPanel.add(new JLabel("<html><h3>" + (i + 1) + ".</h3></html>", SwingConstants.RIGHT) {
                            @Override
                            public Dimension getPreferredSize() {
                                return new Dimension(30, 40);
                            }
                        }, new GBC(0, i + 1, 1, 1).setFill(GBC.BOTH).setWeight(0, 0));
                        jPanel.add(new JLabel("<html><h3>" + settings.getScores().getHighScoreEntries()[i].getName() + "</h3></html>", SwingConstants.CENTER) {
                            @Override
                            public Dimension getPreferredSize() {
                                return new Dimension(130, 40);
                            }
                        }, new GBC(1, i + 1, 2, 1).setFill(GBC.BOTH).setWeight(1, 0));
                        jPanel.add(new JLabel("<html><h3>" + settings.getScores().getHighScoreEntries()[i].getScore() + "</h3></html>", SwingConstants.RIGHT) {
                            @Override
                            public Dimension getPreferredSize() {
                                return new Dimension(60, 40);
                            }
                        }, new GBC(3, i + 1, 1, 1).setFill(GBC.BOTH).setWeight(0, 0).setInsetsH(0, 20));
                        center.add(jPanel);
                    }
                }
                south.add(new JButton(new AbstractAction(Dictionary.OK) {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        informations.setVisible(false);
                    }
                }));
                if (!settings.getScores().isEmpty())
                    south.add(new JButton(new AbstractAction(Dictionary.RESET) {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            settings.resetHighScrores();
                            informations.setVisible(false);
                            showDialog(Dictionary.HIGHSCORE);
                        }
                    }));
                informations.pack();
                break;
            case Dictionary.SETTINGS:
                SettingsPanel settingsPanel = new SettingsPanel(informations, settings.getPropertiesActive(), settings.getPropertiesMaxima());
                center = null;
                informations.add(settingsPanel, BorderLayout.CENTER);
                south.add(new JButton(new AbstractAction(Dictionary.OK) {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        settings.addNewSettings(settingsPanel.getNewSettings());
                        if (centrum instanceof NewGamePanel) startNewGame();
                        informations.setVisible(false);
                    }
                }));
                south.add(new JButton(new AbstractAction(Dictionary.CANCEL) {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        informations.setVisible(false);
                    }
                }));
                south.add(new JButton(new AbstractAction(Dictionary.RESET) {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        settings.resetSettings();
                        informations.setVisible(false);
                        showDialog(Dictionary.SETTINGS);
                    }
                }));
                break;
        }
        if (center != null) informations.add(center, BorderLayout.CENTER);
        informations.add(south, BorderLayout.SOUTH);
        informations.pack();
        informations.setLocationRelativeTo(centrum);
        informations.setVisible(true);
    }

    private void exitConfirmation(Component component) {
        if (JOptionPane.showConfirmDialog(component, "Czy napewno chcesz wyjść?", "Potwierdzenie wyjścia", JOptionPane.YES_NO_OPTION) == JOptionPane.OK_OPTION)
            System.exit(0);
    }
}