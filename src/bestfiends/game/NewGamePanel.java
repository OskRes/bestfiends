package bestfiends.game;

import bestfiends.libs.Settings;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseListener;

class NewGamePanel extends JPanel {
    final GameData data;
    private final GameArrayBallsPanel game;

    NewGamePanel(int[] settings, MouseListener childListener) {
        setLayout(new BorderLayout());
        if (settings[0] == Settings.GAME_MODE_MOVES_INT) this.data = GameData.createMoveGameData(settings[1]);
        else this.data = GameData.createTimeGameData(settings[1]);

        TopPanel topPanel = new TopPanel(data);
        topPanel.addMouseListener(childListener);
        Thread topPanelRefreshThread = new Thread(topPanel);
        topPanelRefreshThread.start();
        topPanelRefreshThread.setName("Thread: Top Panel");
        JPanel resizePanel = new JPanel(new GridBagLayout());
        game = new GameArrayBallsPanel(settings, childListener, data);
        resizePanel.add(game);
        add(resizePanel, BorderLayout.CENTER);
        add(topPanel, BorderLayout.NORTH);
    }

    void stopGameThreads() {
        data.endGame();
        game.endGame();
    }

    boolean isGamePossible() {
        return data.getRemained() > 0;
    }

    @Override
    protected void paintComponent(Graphics g) {
        Graphics2D graphics2D = (Graphics2D) g;
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        super.paintComponent(graphics2D);
    }
}
