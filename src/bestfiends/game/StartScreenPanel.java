package bestfiends.game;

import javax.swing.*;
import java.awt.*;

class StartScreenPanel extends JPanel {
    static final int ORIGINAL_SIZE = 0;
    static final int STRETCH = 1;
    static final int STRETCH_PROPORTIONAL = 2;


    private ImageIcon image;
    private Color color;
    private boolean correctSource;
    private int ratioFactor = 1;

    StartScreenPanel(int ratioFactor) {
        this.ratioFactor = ratioFactor;
        image = new ImageIcon(getClass().getResource("/bestfiends/graphs/large_icon.png"));
        color = new Color(255, 255, 255);
        correctSource = image.getImageLoadStatus() == MediaTracker.COMPLETE;
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        Graphics2D graphics2D = (Graphics2D) g;
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        if (!correctSource) {
            setBackground(color);
            graphics2D.drawString("Graphics file not found", 100, 100);
        } else
            switch (ratioFactor) {
                case ORIGINAL_SIZE:
                    graphics2D.drawImage(image.getImage(), 0, 0, image.getIconWidth(), image.getIconHeight(), color, null);
                    break;
                case STRETCH:
                    graphics2D.drawImage(image.getImage(), 0, 0, getWidth(), getHeight(), color, null);
                    break;
                case STRETCH_PROPORTIONAL:
                    int wh = getWidth() < getHeight() ? getWidth() : getHeight();
                    graphics2D.drawImage(image.getImage(), 0, 0, wh, wh, color, null);
                    break;
            }
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(500, 500);
    }
}
