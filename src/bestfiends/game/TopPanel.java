package bestfiends.game;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseListener;

/**
 * Created by sonic on 12.03.16.
 * panel on top with score
 */
class TopPanel extends JPanel implements Runnable {
    private final JLabel movements = new JLabel();
    private final JLabel actualPoints = new JLabel();
    private final JLabel totalPoints = new JLabel();
    private GameData gameData;
    private boolean run = true;

    TopPanel(GameData gameData) {
        setLayout(new GridLayout(1, 3));
        add(movements);
        add(actualPoints);
        add(totalPoints);
        this.gameData = gameData;
        refreshData();
    }

    private void refreshData() {
        movements.setText(gameData.getRemainedAsString());
        actualPoints.setText("Za ten ruch: " + Integer.toString(gameData.getPoints4move()));
        totalPoints.setText("Suma punktów: " + Integer.toString(gameData.getTotalPoints()));
    }

    @Override
    public void run() {
        try {
            while (run) {
                Thread.sleep(250);
                refreshData();
                if (gameData.getRemained() <= 0) {
                    run = false;
                    getListeners(MouseListener.class)[0].mouseClicked(null);
                }
                validate();
                repaint();
            }
        } catch (InterruptedException e) {
            run = false;
        }
    }
}
