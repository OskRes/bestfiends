package bestfiends.libs;

import java.util.concurrent.ThreadFactory;

/**
 * Created by Iwona on 06.04.2016.
 * Create new ThreadFactory with specific Thread name
 */
public class NamedThreadFactoryBuilder implements ThreadFactory {
    private final String name;
    private int number = 0;

    public NamedThreadFactoryBuilder(String name) {
        this.name = name;
    }

    @Override
    public Thread newThread(Runnable r) {
        number++;
        return new Thread(r, name + number);
    }
}
