package bestfiends.libs;

/**
 * Created by Iwona on 16.03.2016.
 * Class witch static Strings for translations
 */
public class Dictionary {
    public static final String NEWGAME = "Nowa gra";
    public static final String MAINMENU = "Menu główne";
    public static final String SETTINGS = "Ustawienia";
    public static final String HIGHSCORE = "Najlepsi";
    public static final String EXIT = "Koniec";

    public static final String MENUNAME = "Gra";

    public static final String HIGHSCORE_IS_EMPTY = "Lista wyników jest pusta";

    public static final String NEWHIGHSCORE = "Nowy rekord";
    public static final String OK = "OK";
    public static final String CANCEL = "Anuluj";

    public static final String RESET = "Resetuj";
    public static final String GAME_MODE_FRAME = "Rodzaj rozgrywki";
    public static final String GAME_MODE_TIME = "Ograniczenie czasowe";

    public static final String GAME_MODE_MOVES = "Ograniczenie ruchowe";
    public static final String MAXTIME_FRAME = "Długość czasu";

    public static final String MAXMOVES_FRAME = "Ilość ruchów";
    public static final String SIZE_FRAME = "Wymiary pola gry";
    public static final String MINBALLS_FRAME = "Minimalna ilość kul";

    public static final String BALLS_COLOR_FRAME = "Ilość kolorów kul";
}
