package bestfiends.libs;

import java.io.*;
import java.util.logging.Logger;

/**
 * Statyczne metody zapisu i odczytu danych dla klas Scores i Statistics
 * Static methods to save and load data for classes Scrores and Statistics
 * skończona
 */
class SaveAndLoadBySerialize {
    public static boolean save(String name, Object objectToSave) {
        try {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(name));
            objectOutputStream.writeObject(objectToSave);
            objectOutputStream.flush();
            objectOutputStream.close();
        } catch (IOException e) {
            Logger.getGlobal().warning(SaveAndLoadBySerialize.class.getName() + ".save: " + e.getLocalizedMessage());
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public static Object load(String name) throws FileNotFoundException {
        FileInputStream fileInputStream = new FileInputStream(name);
        try {
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            Object out = objectInputStream.readObject();
            objectInputStream.close();
            fileInputStream.close();
            return out;
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static boolean delete(String name) {
        File file = new File(name);
        return file.delete();
    }
}
