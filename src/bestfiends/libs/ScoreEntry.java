package bestfiends.libs;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public final class ScoreEntry implements Comparable<ScoreEntry>, Serializable {
    private final String name;
    private final int score;
    private final Date date;

    public ScoreEntry(String name, int score) {
        this.name = name;
        this.score = score;
        this.date = new Date();
    }

    @Override
    public int hashCode() {
        return name.hashCode() + ((score + date.hashCode()) / 2) + 2;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof ScoreEntry) {
            ScoreEntry obj_cast = (ScoreEntry) obj;
            if (date.equals(obj_cast.getDate()) && (hashCode() == obj_cast.hashCode()))
                return true;
        }
        return false;
    }

    @Override
    public String toString() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.YYYY HH:mm:ss");
        String outputSeparator = "\t-\t";
        return name + outputSeparator + score + outputSeparator + simpleDateFormat.format(date);
    }

    public String getName() {
        return name;
    }

    public Date getDate() {
        return date;
    }

    public int getScore() {
        return score;
    }

    @Override
    public int compareTo(ScoreEntry o) {
        if (score == o.getScore())
            if (getDate().equals(o.getDate())) return -1;
            else return (o.getDate().compareTo(getDate()));
        return (score < o.getScore() ? -1 : 1);
    }
}
