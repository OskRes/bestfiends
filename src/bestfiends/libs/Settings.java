package bestfiends.libs;

import java.io.*;
import java.util.InvalidPropertiesFormatException;
import java.util.Properties;
import java.util.logging.Logger;

public class Settings {
    public static final String SET_GAME_MODE = "setgamemode";
    public static final String GAME_MODE_MOVES = "GMmoves";
    public static final String GAME_MODE_TIME = "GMtime";

    public static final int GAME_MODE_MOVES_INT = 1;
    public static final int GAME_MODE_TIME_INT = 2;

    public static final String MAX_MOVES = "MAXmoves";
    public static final String MAX_TIME = "MAXtime";

    public static final String MINIMUM_BALLS = "minimum_balls";

    public static final String ARRAY_SIZE_WIDTH = "width";
    public static final String ARRAY_SIZE_HEIGHT = "height";

    public static final String DIFFERENT_COLORS = "diffCol";

    private final Properties propertiesDefault;
    private final Properties propertiesMaxima;
    private final String settingsFilename = "settings.xml";
    private final String settingsPatch;
    private final String savesPatch = "save/";
    private Properties propertiesActive;
    private Scores scores = null;
    private boolean settingsEdited;

    {
        propertiesDefault = new Properties();
        propertiesDefault.setProperty(SET_GAME_MODE, GAME_MODE_MOVES);
        propertiesDefault.setProperty(MAX_MOVES, "10");
        propertiesDefault.setProperty(MAX_TIME, "60");
        propertiesDefault.setProperty(MINIMUM_BALLS, "3");
        propertiesDefault.setProperty(DIFFERENT_COLORS, "5");
        propertiesDefault.setProperty(ARRAY_SIZE_WIDTH, "7");
        propertiesDefault.setProperty(ARRAY_SIZE_HEIGHT, "7");

        propertiesMaxima = new Properties();
        propertiesMaxima.setProperty(MAX_MOVES, "20");
        propertiesMaxima.setProperty(MAX_TIME, "180");
        propertiesMaxima.setProperty(MINIMUM_BALLS, "8");
        propertiesMaxima.setProperty(DIFFERENT_COLORS, "5");
        propertiesMaxima.setProperty(ARRAY_SIZE_WIDTH, "10");
        propertiesMaxima.setProperty(ARRAY_SIZE_HEIGHT, "10");
    }

    public Settings(String settingsPatch) {
        if (settingsPatch != null) this.settingsPatch = settingsPatch;
        else this.settingsPatch = "";

        File dataPatch = new File(settingsPatch);
        if (!dataPatch.exists()) dataPatch.mkdir();
        dataPatch = new File(settingsPatch + savesPatch);
        if (!dataPatch.exists()) dataPatch.mkdir();

        propertiesActive = new Properties();
        propertiesActive.putAll(propertiesDefault);
        propertiesActive.putAll(loadSettingsFromFile());
        scores = loadScoresFromFile();
    }

    public static void showData(Properties properties) {
        System.out.println("-----");
        System.out.println(SET_GAME_MODE + " " + properties.getProperty(SET_GAME_MODE));
        System.out.println(MAX_MOVES + " " + properties.getProperty(MAX_MOVES));
        System.out.println(MAX_TIME + " " + properties.getProperty(MAX_TIME));
        System.out.println(MINIMUM_BALLS + " " + properties.getProperty(MINIMUM_BALLS));
        System.out.println(DIFFERENT_COLORS + " " + properties.getProperty(DIFFERENT_COLORS));
        System.out.println(ARRAY_SIZE_WIDTH + " " + properties.getProperty(ARRAY_SIZE_WIDTH));
        System.out.println(ARRAY_SIZE_HEIGHT + " " + properties.getProperty(ARRAY_SIZE_HEIGHT));
        System.out.println("-----");
    }

    private Properties loadSettingsFromFile() {
        Properties propertiesFromFile = new Properties();

        try (InputStream inputStream = new FileInputStream(settingsPatch + settingsFilename)) {
            propertiesFromFile.loadFromXML(inputStream);
        } catch (InvalidPropertiesFormatException e) {
            File file = new File(settingsPatch + settingsFilename);
            Logger.getGlobal().fine("Uszkodzony plik ustawień");
            if (file.delete()) Logger.getGlobal().warning("Błąd usuwania ustawień");
        } catch (FileNotFoundException e) {
            Logger.getGlobal().fine("Nowy plik ustawień");
        } catch (IOException e) {
            Logger.getGlobal().warning(Settings.class.getName() + ".loadSettingsFromFile: " + e.getLocalizedMessage());
            e.printStackTrace();
        }
        return propertiesFromFile;
    }

    private boolean saveSettingsToFile() {
        try (FileOutputStream outputStream = new FileOutputStream(settingsPatch + settingsFilename)) {
            propertiesActive.storeToXML(outputStream, "last used settings " + System.currentTimeMillis());
        } catch (IOException e) {
            Logger.getGlobal().warning(Settings.class.getName() + ".saveSettingsFromFile: " + e.getLocalizedMessage());
            e.printStackTrace();
            return false;
        }
        settingsEdited = true;
        return true;
    }

    private Scores loadScoresFromFile() {
        Scores scores = null;
        try {
            Object o = SaveAndLoadBySerialize.load(settingsPatch + savesPatch + Integer.toString(generateSettingsHash()));
            if (o instanceof Scores) scores = (Scores) o;
            if (scores == null) throw new FileNotFoundException();
            if (!Integer.toString(scores.hashCode()).equals(propertiesActive.getProperty(Integer.toString(generateSettingsHash()), "none"))) {
                throw new FileNotFoundException();
            }
            Logger.getGlobal().fine("Wyniki odczytane z pliku");
        } catch (FileNotFoundException e) {
            Logger.getGlobal().fine("Utworzono nową listę wyników");
            return new Scores();
        }
        return scores;
    }

    public boolean saveScoresToFile() {
        propertiesActive.setProperty(Integer.toString(generateSettingsHash()), Integer.toString(scores.hashCode()));
        saveSettingsToFile();
        return !scores.isEmpty() && SaveAndLoadBySerialize.save(settingsPatch + savesPatch + Integer.toString(generateSettingsHash()), scores);
    }

    public Scores getScores() {
        if (settingsEdited) {
            settingsEdited = false;
            scores = loadScoresFromFile();
        }
        return scores;
    }

    private int generateSettingsHash() {
        int tmp;
        if (propertiesActive.getProperty(SET_GAME_MODE).equals(GAME_MODE_MOVES)) {
            tmp = (GAME_MODE_MOVES_INT << 28);
            tmp += (Integer.valueOf(propertiesActive.getProperty(MAX_MOVES)) << 20);
        } else if (propertiesActive.getProperty(SET_GAME_MODE).equals(GAME_MODE_TIME)) {
            tmp = (GAME_MODE_TIME_INT << 28);
            tmp += (Integer.valueOf(propertiesActive.getProperty(MAX_TIME)) << 20);
        } else {
            propertiesActive = new Properties(propertiesDefault);
            return generateSettingsHash();
        }

        tmp += (Integer.valueOf(propertiesActive.getProperty(ARRAY_SIZE_WIDTH)) << 15);
        tmp += (Integer.valueOf(propertiesActive.getProperty(ARRAY_SIZE_HEIGHT)) << 10);
        tmp += (Integer.valueOf(propertiesActive.getProperty(MINIMUM_BALLS)) << 5);
        tmp += Integer.valueOf(propertiesActive.getProperty(DIFFERENT_COLORS));
        return tmp;
    }

    public void addNewSettings(Properties newSettings) {
        mergePropeties(propertiesActive, newSettings);
        saveSettingsToFile();
    }

    public void resetSettings() {
        mergePropeties(propertiesActive, propertiesDefault);
        saveSettingsToFile();
    }

    public void resetHighScrores() {
        SaveAndLoadBySerialize.delete(settingsPatch + savesPatch + Integer.toString(generateSettingsHash()));
        settingsEdited = true;
    }

    private void mergePropeties(Properties master, Properties newValues) {
        if (newValues == null || master == null) return;
        master.setProperty(SET_GAME_MODE, newValues.getProperty(SET_GAME_MODE, propertiesDefault.getProperty(SET_GAME_MODE)));
        master.setProperty(MAX_MOVES, newValues.getProperty(MAX_MOVES, propertiesDefault.getProperty(MAX_MOVES)));
        master.setProperty(MAX_TIME, newValues.getProperty(MAX_TIME, propertiesDefault.getProperty(MAX_TIME)));
        master.setProperty(MINIMUM_BALLS, newValues.getProperty(MINIMUM_BALLS, propertiesDefault.getProperty(MINIMUM_BALLS)));
        master.setProperty(DIFFERENT_COLORS, newValues.getProperty(DIFFERENT_COLORS, propertiesDefault.getProperty(DIFFERENT_COLORS)));
        master.setProperty(ARRAY_SIZE_WIDTH, newValues.getProperty(ARRAY_SIZE_WIDTH, propertiesDefault.getProperty(ARRAY_SIZE_WIDTH)));
        master.setProperty(ARRAY_SIZE_HEIGHT, newValues.getProperty(ARRAY_SIZE_HEIGHT, propertiesDefault.getProperty(ARRAY_SIZE_HEIGHT)));
    }

    public Properties getPropertiesActive() {
        Properties properties = new Properties();
        mergePropeties(properties, propertiesActive);
        return properties;
    }

    public int[] getSettings() {
        int[] tmp = new int[6];
        if (propertiesActive.getProperty(SET_GAME_MODE).equals(GAME_MODE_MOVES)) {
            tmp[0] = GAME_MODE_MOVES_INT;
            tmp[1] = Integer.valueOf(propertiesActive.getProperty(MAX_MOVES));
        } else {
            tmp[0] = GAME_MODE_TIME_INT;
            tmp[1] = Integer.valueOf(propertiesActive.getProperty(MAX_TIME));
        }
        tmp[2] = Integer.valueOf(propertiesActive.getProperty(ARRAY_SIZE_WIDTH));
        tmp[3] = Integer.valueOf(propertiesActive.getProperty(ARRAY_SIZE_HEIGHT));
        tmp[4] = Integer.valueOf(propertiesActive.getProperty(MINIMUM_BALLS));
        tmp[5] = Integer.valueOf(propertiesActive.getProperty(DIFFERENT_COLORS));
        return tmp;
    }

    public Properties getPropertiesMaxima() {
        Properties properties = new Properties();
        mergePropeties(properties, propertiesMaxima);
        return properties;
    }
}
