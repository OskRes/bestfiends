package bestfiends.libs;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Iterator;
import java.util.TreeSet;

/**
 * Scores - Tablica wyników.
 * ScoreEntry - Wpis tablicy.
 * skończona
 */
public final class Scores implements Serializable {
    private static int HIGH_SCORE_SIZE = 10;
    private final TreeSet<ScoreEntry> highscores;

    public Scores() {
        highscores = new TreeSet<>();
    }

    public boolean addNewHS(ScoreEntry newScoreEntry) {
        if (highscores.add(newScoreEntry)) {
            while (highscores.size() > HIGH_SCORE_SIZE) {
                Iterator<ScoreEntry> scoreEntryIterator = highscores.iterator();
                scoreEntryIterator.next();
                scoreEntryIterator.remove();
            }
            return true;
        } else return false;
    }

    public boolean isNewHighScore(int newScorePoints) {
        return highscores.size() < HIGH_SCORE_SIZE || newScorePoints > highscores.iterator().next().getScore();
    }

    public boolean isEmpty() {
        return highscores.isEmpty();
    }

    @Override
    public int hashCode() {
        return highscores.hashCode() + HIGH_SCORE_SIZE;
    }

    public ScoreEntry[] getHighScoreEntries() {
        ScoreEntry[] tmp = highscores.toArray(new ScoreEntry[highscores.size()]);
        Arrays.sort(tmp, (o1, o2) -> o2.compareTo(o1));
        return tmp;
    }
}

