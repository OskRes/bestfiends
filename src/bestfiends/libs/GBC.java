package bestfiends.libs;

import java.awt.*;

public class GBC extends GridBagConstraints {
    public GBC(int gridx, int gridy, int gridwidth, int gridheight) {
        this.gridx = gridx;
        this.gridy = gridy;
        this.gridwidth = gridwidth;
        this.gridheight = gridheight;
        setWeight(100, 100);
    }

    public GBC(int gridx, int gridy) {
        this.gridx = gridx;
        this.gridy = gridy;
        this.gridwidth = 1;
        this.gridheight = 1;
    }

    public GBC setWeight(double x, double y) {
        this.weightx = x;
        this.weighty = y;
        return this;
    }

    public GBC setAnchor(int i) {
        anchor = i;
        return this;
    }

    public GBC setFill(int i) {
        fill = i;
        return this;
    }

    public GBC setInsetsH(int left, int right) {
        insets.left = left;
        insets.right = right;
        return this;
    }

    public GBC setInsetsV(int up, int down) {
        insets.top = up;
        insets.bottom = down;
        return this;
    }

    public GBC setIpad(int minX, int minY) {
        ipadx = minX;
        ipady = minY;
        return this;
    }
}
