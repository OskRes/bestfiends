package bestfiends;

import bestfiends.game.MasterWindow;

import javax.swing.*;
import java.awt.*;
import java.util.logging.*;

/**
 * Klasa główna
 */
@SuppressWarnings("WeakerAccess")
public class BestFiends {

    public static void main(String[] args) {
        EventQueue.invokeLater(new GraphThread());

        Logger.getGlobal().setLevel(Level.ALL);
        LogManager.getLogManager().reset();
        Handler handler = new ConsoleHandler();
        handler.setLevel(Level.WARNING);
        if (args.length > 0) {
            Integer loggerLevel = new Integer(args[0]);
            if (loggerLevel > 0 && loggerLevel < 8) {
                switch (loggerLevel) {
                    case 1:
                        handler.setLevel(Level.FINEST);
                        break;
                    case 2:
                        handler.setLevel(Level.FINER);
                        break;
                    case 3:
                        handler.setLevel(Level.FINE);
                        break;
                    case 4:
                        handler.setLevel(Level.CONFIG);
                        break;
                    case 5:
                        handler.setLevel(Level.INFO);
                        break;
                    case 6:
                        handler.setLevel(Level.WARNING);
                        break;
                    case 7:
                        handler.setLevel(Level.SEVERE);
                        break;
                }
            }
        }
        Logger.getGlobal().addHandler(handler);
//        System.out.println("Poziom logowania: " + handler.getLevel().getName());
    }
}

class GraphThread implements Runnable {
    @Override
    public void run() {
        JFrame mainWindow = new MasterWindow();
        mainWindow.setVisible(true);
    }
}